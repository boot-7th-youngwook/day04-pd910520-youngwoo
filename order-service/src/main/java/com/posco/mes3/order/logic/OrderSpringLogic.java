package com.posco.mes3.order.logic;

import org.springframework.stereotype.Service;

import com.posco.mes3.order.lifecycle.StoreLifeCycle;

@Service
public class OrderSpringLogic extends OrderLogic{

	public OrderSpringLogic(StoreLifeCycle storeLifeCycle) {
		super(storeLifeCycle);
	}
}
