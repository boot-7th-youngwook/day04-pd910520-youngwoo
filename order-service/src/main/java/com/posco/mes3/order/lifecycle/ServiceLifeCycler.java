package com.posco.mes3.order.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.order.spec.OrderService;

@Component
public class ServiceLifeCycler implements ServiceLifeCycle {

	private final OrderService orderService;
	
	public ServiceLifeCycler(OrderService orderService) {
		this.orderService = orderService;
	}
	
	
	@Override
	public OrderService requestOrderService() {
		// TODO Auto-generated method stub
		return this.orderService;
	}

}
