package com.posco.mes3.order.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.posco.mes3.order.entity.Order;
import com.posco.mes3.order.lifecycle.ServiceLifeCycle;
import com.posco.mes3.order.spec.OrderService;

@RestController
public class OrderResource {

	private final OrderService orderService;
	
	public OrderResource(ServiceLifeCycle serviceLifeCycle) {
		this.orderService = serviceLifeCycle.requestOrderService();
	}
	
	@PostMapping(value= {"","/"})
	public void registerOrder(@RequestBody Order order) {
		// TODO Auto-generated method stub
		System.out.println("Order"+order.getOrderNo());
		orderService.registerOrder(order);
	}

	@GetMapping(value="/{orderNo}")
	public Order findOrder(@PathVariable String orderNo) {
		System.out.println("Order"+orderNo);
		return orderService.findOrder(orderNo);
	}

}
