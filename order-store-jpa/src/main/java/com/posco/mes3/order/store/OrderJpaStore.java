package com.posco.mes3.order.store;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.posco.mes3.order.entity.Order;
import com.posco.mes3.order.store.jpo.OrderJpo;
import com.posco.mes3.order.store.repository.OrderRepository;

@Repository
public class OrderJpaStore implements OrderStore {
	
	private final OrderRepository orderRepository;
	
	public OrderJpaStore(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}
	
	@Override
	public void create(Order order) {
		// TODO Auto-generated method stub
		this.orderRepository.save(new OrderJpo(order));
	}

	@Override
	public Order retrive(String orderNo) {
		// TODO Auto-generated method stub
		Optional<OrderJpo> orderJpo = this.orderRepository.findById(orderNo);
		
		if( ! orderJpo.isPresent()) {
			throw new NoSuchElementException(String.format("Order(%s) is not found", orderNo));
		}
		 return orderJpo.get().toDomain();
	}

}
