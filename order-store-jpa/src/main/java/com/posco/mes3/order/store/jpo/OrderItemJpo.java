package com.posco.mes3.order.store.jpo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.posco.mes3.order.entity.OrderItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="TB_ORDER_ITEM")
@Getter
@Setter
@NoArgsConstructor
public class OrderItemJpo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;

	@Column(name="PRODUCT_NO")
	private String productNo;
	@Column(name="PRODUCT_NAME")
	private String productName;
	@Column(name="PRICE")
	private int price;
	@Column(name="QUANTITY")
	private int quantity;
	
	@ManyToOne
	private OrderJpo orderJpo;
	
	public OrderItemJpo(OrderJpo orderJpo,OrderItem orderItem) {
		this.orderJpo = orderJpo;
		BeanUtils.copyProperties(orderItem, this);
	}
	
	public OrderItem toDomain() {
		OrderItem orderItem = new OrderItem();
		BeanUtils.copyProperties(this, orderItem);
		return orderItem;
	}
}
