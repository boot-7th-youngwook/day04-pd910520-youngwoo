package com.posco.mes3.order.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.order.store.OrderStore;

@Component
public class StoreLifeCycler implements StoreLifeCycle {

	private final OrderStore orderStore;
	
	//spring di
	public StoreLifeCycler(OrderStore orderStore) {
		this.orderStore = orderStore;
	}
	
	@Override
	public OrderStore requestOrderStore() {
		// TODO Auto-generated method stub
		return orderStore;
	}

}
