package com.posco.mes3.order.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.posco.mes3.order.store.jpo.OrderJpo;

public interface OrderRepository extends JpaRepository<OrderJpo,String>{

}
