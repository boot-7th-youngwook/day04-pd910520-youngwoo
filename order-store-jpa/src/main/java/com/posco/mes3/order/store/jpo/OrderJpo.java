package com.posco.mes3.order.store.jpo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.posco.mes3.order.entity.Order;
import com.posco.mes3.order.entity.OrderItem;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="TB_ORDER")
public class OrderJpo {

	@Id
	@Column(name="ORDER_NO")
	private String orderNo;
	
	@Column(name="ORDER_TM")
	private long time;
	
	@OneToMany(fetch= FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "orderJpo")
	private List<OrderItemJpo> orderItemJpos;
	
	public OrderJpo() {
		this.orderItemJpos = new ArrayList<OrderItemJpo>();
	}
	
	public OrderJpo(Order order) {
		this();
		BeanUtils.copyProperties(order, this);
		for(OrderItem orderItem : order.getItems()) {
			this.orderItemJpos.add(new OrderItemJpo(this,orderItem));
		}
	}
	
	public Order toDomain() {
		Order order = new Order();
		BeanUtils.copyProperties(this, order);
		for(OrderItemJpo itemJpo : this.orderItemJpos) {
			order.addItem(itemJpo.toDomain());
		}
		return order;
			
	}
}
