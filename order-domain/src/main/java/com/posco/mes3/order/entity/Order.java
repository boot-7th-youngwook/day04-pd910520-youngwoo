package com.posco.mes3.order.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {

	private String orderNo;
	private long time;
	
	private List<OrderItem> items;
	
	public Order() {
		this.items = new ArrayList<OrderItem>();
	}
	
	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis());
	}
	
	public void addItem(OrderItem orderItem) {
		if(this.items.size() >= 2) {
			throw new RuntimeException("Error");
		}
		this.items.add(orderItem);
	}
	
	public void removeItem(String productNo) {
		
	}
}
