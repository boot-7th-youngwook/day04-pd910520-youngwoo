package com.posco.mes3.order.spec;

import com.posco.mes3.order.entity.Order;

public interface OrderService {

	public void registerOrder(Order order);
	public Order findOrder(String orderNo);
}
