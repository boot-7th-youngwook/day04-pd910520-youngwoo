package com.posco.mes3.order.store;

import com.posco.mes3.order.entity.Order;

public interface OrderStore {

	public void create(Order order);
	
	public Order retrive(String orderNo);
}
