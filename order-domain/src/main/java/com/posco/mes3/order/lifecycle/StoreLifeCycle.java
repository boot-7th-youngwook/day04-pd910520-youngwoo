package com.posco.mes3.order.lifecycle;

import com.posco.mes3.order.store.OrderStore;

public interface StoreLifeCycle {

	public OrderStore requestOrderStore();
}
