package com.posco.mes3.order.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OrderItem {

	private String productNo;
	
	private String productName;
	
	private int price;
	
	private int quantity;
	
	
	public int getSubTotal() {
		return price * quantity ;
	}
	
}
