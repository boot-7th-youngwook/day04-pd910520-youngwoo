package com.posco.mes3.order.logic;

import com.posco.mes3.order.entity.Order;
import com.posco.mes3.order.lifecycle.StoreLifeCycle;
import com.posco.mes3.order.spec.OrderService;
import com.posco.mes3.order.store.OrderStore;

public class OrderLogic implements OrderService {

	
	private final OrderStore orderStore;
	
	public OrderLogic(StoreLifeCycle storeLifeCycle) {
		this.orderStore = storeLifeCycle.requestOrderStore();
	}
	
	@Override
	public void registerOrder(Order order) {
		orderStore.create(order);
	}

	@Override
	public Order findOrder(String orderNo) {
		return orderStore.retrive(orderNo);
	}

}
