package com.posco.mes3.order.lifecycle;

import com.posco.mes3.order.spec.OrderService;

public interface ServiceLifeCycle {

	public OrderService requestOrderService();
	
}
